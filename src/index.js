const fs = require("fs");

const hashtags = async (fileName) => {
  const filePath = fileName || process.argv[2] || "./data/input1.txt";
  const res = {};

  if (!(await fs.existsSync(filePath))) {
    return "Pas de trending topic.";
  }

  // Read and allocate buffer with the size of the file
  const stats = await fs.statSync(filePath);

  const fd = await fs.openSync(filePath, "r");

  const buffer = Buffer.alloc(stats.size);

  await fs.readSync(fd, buffer, 0, buffer.length);

  // Get the data and split if by newline to get each hashtag
  const fileContant = buffer.toString();
  const file = fileContant.split("\n");

  // Just go over the entire file
  for (let i = 0; i < file.length; i += 1) {
    if (res[file[i]]) {
      res[file[i]].push(i);
    } else {
      // Create new array with the first index of this hashtag
      res[file[i]] = [i];
    }

    // Clear old hashtags if time in trendings is over
    for (let index = 0; index < res[file[i]].length; index += 1) {
      if (res[file[i]][index] < i - 60) {
        res[file[i]].shift();
      }
    }
    // We got a trending here
    if (res[file[i]].length >= 40) {
      return file[i];
    }
  }
  return "Pas de trending topic.";
};

const main = async () => {
  hashtags().then((res) => console.log(res));
};

main();

module.exports.hashtags = hashtags;
