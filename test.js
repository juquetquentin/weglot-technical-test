const test = require("ava");
const index = require("./src/index");

test("input n°1", async (t) => {
  const res = await index.hashtags("./data/input1.txt");
  t.is(res, "#economie");
});

test("input n°2", async (t) => {
  const res = await index.hashtags("./data/input2.txt");
  t.is(res, "#politique");
});

test("input n°3", async (t) => {
  const res = await index.hashtags("./data/input3.txt");
  t.is(res, "Pas de trending topic.");
});

test("empty file", async (t) => {
  const res = await index.hashtags("./data/input4.txt");
  t.is(res, "Pas de trending topic.");
});

test("no file", async (t) => {
  const res = await index.hashtags("./data/input.txt");
  t.is(res, "Pas de trending topic.");
});

test("bar", async (t) => {
  const bar = Promise.resolve("bar");
  t.is(await bar, "bar");
});
